import models.Account;
import models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Anh", 20);
        Customer customer2 = new Customer(2, "Hoàng", 10);
    System.out.println(" Customer 1");
        System.out.println(customer1.toString());
        System.out.println(" Customer 2");
        System.out.println(customer2.toString());

        Account account1 = new Account(1, customer1, 10.0);
        Account account2 = new Account(2, customer2, 8500.0);
        
        
        System.out.println(" Account 1");
        System.out.println(account1.toString());
        System.out.println(" Account 2");
        System.out.println(account2.toString());

    }
}
