package models;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;
    public Account() {
    }
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName(String name){
        return customer.getName();

    }
    // tổng tiền gửi 
    public Account deposit(double amount){
         this.balance += amount;
        return this;
    }

    public Account withdraw(double amount) {
        if (this.balance >= amount) {
            this.balance -= amount;
        } else {
            System.out.println("Amount withdraw exceeds the current balance!");
        }
        return this;
    }
    @Override
    public String toString() {
        double roundOff = (double) Math.round(balance *100) /100; 
        return String.format("Name (id = %s) balane = $%s",id, roundOff);
    }
    
}
